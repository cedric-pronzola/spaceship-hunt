## Disponible sur : http://www.cedric-famibelle.fr/spaceship-hunt

# Reproduire la mécanique du jeu Duck Hunt en multijoueur, avec un joueur qui contrôle le canard et l'autre qui doit lui tirer dessus.

[![Gif Books'IFA]( http://www.cedric-famibelle.fr/space/space.gif)](https://www.youtube.com/watch?v=mV6yf6V9ROI)