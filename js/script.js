
$(function () {

    /**
     * Initialisation des sons
     */
    ion.sound({
        sounds: [
            {
                // se déclenche au click dans le #gameBoard
                name: "staple_gun",
                volume: 0.5
            },
            {
                // compte à rebours Mario Kart
                name: "count_down_mk",
                volume: 0.5
            },
            {
                // fin du compte à rebours Mario Kart
                name: "count_down_mk_end",
                volume: 0.5
            },
            {
                // voix de Mario dès que le joueur au clavier gagne
                name: "keyboard_win",
                volume: 0.5
            },
            {
                // se déclenche quand la souris gagne
                name: "mouse_win",
                volume: 0.5
            },
            {
                // se déclenche quand le joueur perd en mode solo
                name: "game_over",
                volume: 0.5
            },
            {
                // se déclenche quand le cheat code est exécuté
                name: "cheat_code",
                volume: 0.5
            },
            {
                // theme ryu street fighter en boucle
                name: "stage_sound",
                volume: 0.7,
                loop: true
            }
        ],
        volume: 0.5,
        path: "sounds/",
        preload: true,
        multiplay: true
    });

    let posTop;                         // position Y de #duck
    let posLeft;                        // position X de #duck
    let player1;                        // nom du joueur 1
    let player2;                        // nom du joueur 2
    let scoreP1 = 0;                    // score du joueur 1
    let scoreP2 = 0;                    // score du joueur 2
    let gameSet = 0;                    // nombre de manche
    let speed = 2.5;                    // vitesse de #duck en mode multi
    let speedIA = 1;                    // vitesse de #duck en mode solo
    let level = 1;                      // level en mode solo
    let endGame = false;                // fin du mode solo
    let winner;                         // nom du gagnant
    let modeMulti;                      // mode multi
    let solo;                           // mode solo
    let noChoice;                       // pas de choix du mode
    let insertAmmo = [];                // tableau contenant la div qui intègre les munitions
    let nbAmmo = 5;                     // nombre de munition
    let $gameSet = $('.gameSet');       // pointe vers la div qui contient le nombre de manche
    let countSec = 3;                   // nombre de seconde avant le début d'une manche/partie
    let keyboardSec = 30;               // nombre de seconde avant la victoire du clavier
    let hitDuck = false;                // boolean #duck touché ? 
    let cheatCode;                      // boolean cheat code ?
    let cheatCodeSlow = false;          // boolean cheat code ?
    let myAnim;
    let gameSoloStart = false;                  // boolean game solo start ?          

    $('#duck').hide();
    $('#details').hide();
    $('#alertMouse').hide();

    /**
     * Choix du mode de jeu
     */
    $('#gameMode').selectmenu({
        select: function(event, ui){
            if(ui.item.value == 0)
            {
                noChoice = true;
            }
            else if(ui.item.value == 1)
            {
                modeMulti = true;
                solo = false;
            }
            else if(ui.item.value == 2)
            {
                solo = true;
                modeMulti = false;
            }
        }
    });

    /**
     * Démarre le jeu au click sur le boutton. Le mode est déterminé grâce au #gameMode
     */
    $('#startGame').click(function(){

        if(noChoice)
        {
            console.log('no choice');
        }
        else if(modeMulti)
        {
            scoreP1 = 0;
            scoreP2 = 0;
            gameSet = 0;
            $('#startGame').attr('disabled', true);
            player1 = player1F();
            player2 = player2F();
            countDown();
            $('#gameMode').selectmenu("option", "disabled", true);
            
        }
        else if(solo)
        {   $('#startGame').attr('disabled', true);
            $('#gameMode').selectmenu("option", "disabled", true);
            countDownSolo();
        }

    })

    /**
     * Prompt qui invite le joueur 1 à entrer son nom
     * @return {string} player1 : Nom du joueur
     */
    function player1F()
    {
        player1 = prompt('Joueur 1 (Clavier) indiquez votre nom : ');
    
        if(typeof(player1) == 'object')
        player1 = 'Clavier';

        return player1;
    }

    /**
     * Prompt qui invite le joueur 2 à entrer son nom
     * @return {string} player2 : Nom du joueur
     */
    function player2F()
    {
        player2 = prompt('Joueur 2 (Souris) indiquez votre nom : ');
    
        if(typeof(player2) == 'object')
        player2 = 'Souris';

        return player2;
    }

    /**
     * Compte à rebours de début de partie Multijoueur. Réglé a 3 secondes
     */
    function countDown()
    {
        $('#timer').html('<h6>Prêt ' + countSec + '</h6>');
        countSec--;
        ion.sound.play('count_down_mk');
        time = setTimeout(countDown, 1000);

        if(countSec < 0)
        {
            $('#timer').html('<h6>Partez</h6>');
            ion.sound.play('count_down_mk_end');
            clearTimeout(time)
            countSec = 3;
            initMulti(player1, player2);
            return;
        }
    }

    /**
     * Compte à rebours de la victoire du keyboard : Réglé a 30 secondes
     */
    function keyboardSecF()
    {

        $('#keyboardSec').html('<p>Victoire de ' + player1 + ' dans : ' + keyboardSec +  ' sec</p>');
        time = setTimeout(keyboardSecF, 1000);
        keyboardSec--;

        if(hitDuck)
        {
            keyboardSec = 30;
            clearTimeout(time)
            hitDuck = false;
        }

        if(keyboardSec < 0)
        {
            clearTimeout(time)
            keyboardSec = 30;
            victoireClavier();
            return;
        }

    }

    /**
     * Fonction appelée à la vitoire du joueur au clavier
     */
    function victoireClavier()
    {
        ion.sound.play('keyboard_win');
        ion.sound.pause('stage_sound');
        gameSet+=1;
        scoreP1+=1;
        $('#gameSet').html(gameSet);
        $('#scoreP1').html(scoreP1);
        $('#duck').css({
            backgroundImage: 'url("./img/ovni_untuched.gif")'
        })

        $('body').off();
        $('#duck').off();
        $('#startGame').attr('disabled', true);

        if(scoreP1 == 2)
        {
            speed = 1.5;
            $('#duck').css({
                backgroundSize: '120px 120px',
                height: '120px',
                width: '120px'
            })
        }

        if(gameSet < 5)
        {
           setTimeout(countDown, 3000);
           $('#alertMouse').html('<div>'+ player1 +' gagne cette manche</div>').fadeIn(300);
           $('#alertMouse').fadeOut(3000);
        }
        else
        {
            winner = (scoreP1 > scoreP2) ? player1 : player2;
            alert('Jeux terminé, victoire de ' + winner);
            ion.sound.stop('stage_sound');
            setTimeout(function(){location.reload()}, 2000);
        }

    }

    /**
     * Début de la partie multijoueur
     * 
     * @param {string} player1 Nom du joueur 1
     * @param {string} player2 Nom du joueur 2
     */
    function initMulti(player1, player2)
    {
        posTop = getRandomInt(80);
        posLeft = getRandomInt(80);
        $('#details').fadeIn(300);

        ion.sound.play('stage_sound');
        keyboardSecF();

        $('#duck').css({
            backgroundImage: 'url("./img/ovni_normal.gif")',
            top: posTop + '%',
            left: posLeft + '%'
        })

        $('#gameBoard').click(function(){
            shoot();
        })

        $('#duck').css({
            borderRadius: 0
        })
        $('#duck').show();

        $('.players').html(`<p id="playerOne">Joueur 1 : ${player1}<br>Score : <span id="scoreP1">${scoreP1}</span></p><p id="playerTwo">Joueur 2 : ${player2}<br>Score : <span id="scoreP2">${scoreP2}</span></p>`);

        $gameSet.html(`Nombre de manche : <span id="gameSet">${gameSet}</span>/5`);
        
        $('body').keydown(function(e){
            switch (e.code) {
    
                case 'ArrowUp':
                posTop -= speed;
                break;
    
                case 'ArrowDown':
                posTop += speed;
                break;
    
                case 'ArrowLeft':
                posLeft -= speed;
                break;
    
                case 'ArrowRight':
                posLeft += speed;
                break;
            
                default:
                break;
            }
            $('#duck').css({
                top: posTop + '%',
                left: posLeft + '%'
            })
    
            if(posLeft < 10) posLeft = 90;
            if(posTop < 10) posTop = 90;
            if(posLeft > 90 ) posLeft = 10;
            if(posTop > 90 ) posTop = 10;
        })
    
        $('#duck').click(function(e){
            hitDuck = true;
            ion.sound.play('mouse_win');
            ion.sound.pause('stage_sound');
            gameSet+=1;
            scoreP2+=1;
            $('#gameSet').html(gameSet);
            $('#scoreP2').html(scoreP2);
            $(this).css({
                backgroundImage: 'url("./img/ovni_touched.gif")'
            });

            $('body').off();
            $('#duck').off();
            if(gameSet < 5)
            {
                $('#anotherSet').attr('disabled', false);
                $('#startGame').attr('disabled', true);
                setTimeout(countDown, 3000);
                $('#alertMouse').html('<div>'+ player2 +' gagne cette manche</div>').fadeIn(300);
                $('#alertMouse').fadeOut(3000);
            }
            else
            {
                winner = (scoreP1 > scoreP2) ? player1 : player2;
                setTimeout(function(){alert('Jeux terminé, victoire de ' + winner)}, 1000) ;
                $('#startGame').attr('disabled', false);
                setTimeout(function(){location.reload()}, 1500);
                ion.sound.stop('stage_sound');
            }
            
            if(scoreP2 == 2)
            {
                speed = 3.5;
                $('#duck').css({
                    backgroundSize: '80px 80px',
                    height: '80px',
                    width: '80px'
                })
            }

            if(scoreP2 == 3)
            {
                speed = 4;
                $('#duck').css({
                    backgroundSize: '50px 50px',
                    height: '50px',
                    width: '50px'
                })
            }

        })
        
    }

    /**
     * Compte à rebours de début de partie. Mode solo. Réglé a 3 secondes.
     */
    function countDownSolo()
    {
        $('#timer').html('<h6>Prêt ' + countSec + '</h6>');
        countSec--;
        ion.sound.play('count_down_mk');
        time = setTimeout(countDownSolo, 1000);
        $('#gameBoard').attr('data-nbClick', false);

        if(countSec < 0)
        {
            $('#timer').html('<h6>Partez</h6>');
            ion.sound.play('count_down_mk_end');
            clearTimeout(time)
            countSec = 3;
            initSolo();
            return;
        }
    }

    
    
    konamiUnilimitedAmmo();
    konamiSlowDuck();

    /**
     * Début de la partie Mode Solo
     */
    function initSolo()
    {
        gameSoloStart = true;
        $('#details').fadeIn(300);
        hitDuck = false;
        nbAmmo = 5;
        cheatCode = false;
        $('#gameBoard').on();

        ammo(nbAmmo);

        ion.sound.play('stage_sound');

        switch (level) {

            case 2:
            speedIA = 0.8;
            break;

            case 4:
            speedIA = 0.5;
            break;

            case 6:
            speedIA = 0.2;
            break;

            case 8:
            speedIA = 0.1;
            break;

            case 10:
            speedIA = 0.08;
            break;
        
            default:
            break;
        }

        if(cheatCodeSlow)
        {
            speedIA *= 2;
            cheatCodeSlow = false
        }

        posTop = getRandomInt(80);
        posLeft = getRandomInt(80);

        $('#level').html(`<h4 class="mr-3">Level : <span id="setLevel">${level}</span></h4>`);

        $('#duck').show();
        $('#duck').css({
            backgroundImage: 'url("./img/ovni_normal.gif")',
            top: posTop + '%',
            left: posLeft + '%'
        });

        myAnim = setInterval(animation, 1);

        $('#duck').click(function(){
            
            hitDuck = true;
            ion.sound.play('mouse_win');
            ion.sound.pause('stage_sound');
            level ++;
            $('#setLevel').text(level);
            $('#duck').css({
                backgroundImage: 'url("./img/ovni_touched.gif")'
            });
            $('#duck').stop(true, false);
            $('#duck').off();
            clearInterval(myAnim);
            setTimeout(countDownSolo, 1000);

        })

    }

    let j = 1;

    $('#gameBoard').click(function(){

        if(gameSoloStart)
        {
            if(!hitDuck)
            {
                if(cheatCode)
                {
                    $(this).attr('data-nbClick', 0);
                }
                else
                {
                    $(this).attr('data-nbClick', j++);
                }
                
                insertAmmo.shift();
                ammo(nbAmmo - $(this).attr('data-nbClick'));
                shoot();
    
                if(typeof(insertAmmo[0]) === 'undefined')
                {
                    endGame = true;
                    clearInterval(myAnim);
                    gameOverSolo();
                }
            }
        }

    })

    /**
     * Fonction appelée quand le joueur n'a plus de munitions
     */
    function gameOverSolo()
    {
        if(endGame)
        {
            ion.sound.stop('stage_sound');
            ion.sound.play('game_over');
            gameSoloStart = false;
            $('#duck').stop(true, false);
            alert('Vous avez perdu au level ' + level);
            setTimeout(function(){location.reload()}, 2000);
            endGame = false;
        }
    }

    /**
     * Ajoute l'image d'un "Bill Ball" à gauche de l'écran. Le nombre dépend de l'argument de la fonction
     * 
     * @param {integer} nbAmmo nombre de munitions à l'initialisation du jeu
     */
    function ammo(nbAmmo)
    {

        for(let i = 0; i < nbAmmo; i++)
        {
            insertAmmo[i] = '<img class="nbAmmo mb-1" src="./img/ammo.png">';
        }

        $('#ammo').html(insertAmmo);

        $('.nbAmmo').css({
            float: 'left',
            width: '40px',
            height: '40px'
        })

    }

    /**
     * Fonction appelée à chaque click dans le #gameBoard.
     * Animation qui change l'arrière plan au click.
     */
    function shoot()
    {
        $('body').animate({
            backgroundColor: '#2a2a2a'
        }, 20, 'linear', function(){
            $('body').css('backgroundColor', 'black');
        });

        ion.sound.play('staple_gun');
    }

    /**
     * Fonction qui permet à l'IA de se déplacer de manière aléatoire
     */
    function animation()
    {
        $('#duck').animate({
            'left': `${getRandomInt(75)}%`,
            'top': `${getRandomInt(75)}%`
        }, (speedIA * 1000));
    }

    /**
     * Fonction qui permet d'obtenir en chiffre aléatoire entre 0 et max
     * 
     * @param {integer} max 
     */
    function getRandomInt(max) {
        return Math.floor(Math.random() * Math.floor(max));
    }

    /**
     * KONAMI CODE MUNITION ILLIMITEES
     * --------------------------------
     * haut, haut, bas, bas, gauche, droite, gauche, droite, b, a
     * 
     */
    function konamiUnilimitedAmmo()
    {
        let allowedKeys = {
            37: 'left',
            38: 'up',
            39: 'right',
            40: 'down',
            65: 'a',
            66: 'b'
        };
      
        let konamiCode = ['up', 'up', 'down', 'down', 'left', 'right', 'left', 'right', 'b', 'a'];
        let konamiCodePosition = 0;
        document.addEventListener('keydown', function(e) {
        let key = allowedKeys[e.keyCode];
        let requiredKey = konamiCode[konamiCodePosition];
    
        if (key == requiredKey && solo == true) 
        {
            konamiCodePosition++;
            if (konamiCodePosition == konamiCode.length) {
                activateCheats();
                konamiCodePosition = 0;
            }
        } 
        else 
        {
            konamiCodePosition = 0;
        }
        });
    }

    /**
     * Fonction appelée dès que le konami code "konamiUnilimitedAmmo()" est lancé
     */
    function activateCheats() {
        cheatCode = true;
        ion.sound.play('cheat_code');
        alert('Munitions illimitées pour ce level');
    }


    /**
     * KONAMI CODE RALENTI #duck
     * --------------------------------
     * haut, haut, haut, haut, bas, bas, bas, bas
     * 
     */
    function konamiSlowDuck()
    {
        let allowedKeys = {
            38: 'up',
            40: 'down'
        };
      
        let konamiCode = ['up', 'up', 'up', 'up', 'down', 'down', 'down', 'down'];
        let konamiCodePosition = 0;
        document.addEventListener('keydown', function(e) {
        let key = allowedKeys[e.keyCode];
        let requiredKey = konamiCode[konamiCodePosition];
    
        if (key == requiredKey && solo == true) 
        {
            konamiCodePosition++;
            if (konamiCodePosition == konamiCode.length) {
                activateCheatSlow();
                konamiCodePosition = 0;
            }
        } 
        else 
        {
            konamiCodePosition = 0;
        }
        });
    }

    /**
     * Fonction appelée dès que le konami code "konamiSlowDuck()" est lancé
     */
    function activateCheatSlow() {
        cheatCodeSlow = true;
        ion.sound.play('cheat_code');
        alert('Vous recommencerez ce level avec un vaisseau plus lent');
        $('#duck').stop(true, false);
        $('#duck').off();
        gameSoloStart = false;
        clearInterval(myAnim);
        setTimeout(countDownSolo, 1000);
    }



});